Requirements:

Criar ambiente virtual:

```python
conda create --name dash python=3.9
pip install dash
pip install jupyter-dash
```

ou 

```bash
conda create --name dash python=3.9
git clone https://gitlab.com/niltonmalves/dash_demo.git
cd dash_demo
conda activate dash
pip install -r requirements.txt
```



------

Dash é um FrameWork que serve para construir Dash Boards para construir aplicações web usando python.

Não é necessário ser especialista em CSS, javascript e html.

[Exemplos de DashBoards: ](https://dash-gallery.plotly.host/Portal/)

Para criar o Dashboard são necessários 3 pilares. Dash Components, Plotly Graphs e The Callbacks



------

**Dash Components**

------

contém : Sliders, check boxs, DropDown to create web interability. Para verificar a lista completa de opções, basta clicar no [link abaixo](https://dash.plotly.com/dash-core-components).


[Documentação do Dash Components](https://dash.plotly.com/dash-core-components)

https://dash.plotly.com/dash-core-components

![components](https://gitlab.com/niltonmalves/dash_demo/-/raw/main/readme_images/components1.PNG)

------

**Plotly Graphs**

------

É o componente que determina o tipo de gráfico que será utilizado. Há uma variedade muito grande de gráficos. Para verificar, basta clicar no link abaixo, que terá acesso à documentação e aos exemplos.

[https://plotly.com/python/](https://plotly.com/python/)

![graphs](https://gitlab.com/niltonmalves/dash_demo/-/raw/main/readme_images/graphs.PNG)

------

**The Callback**

------

Conecta o Dash Components com Plotly Graphs. Veja a documentação no link abaixo.

[Documentação do callbacks ](https://dash.plotly.com/basic-callbacks)



```python
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H6("Change the value in the text box to see callbacks in action!"),
    html.Div([
        "Input: ",
        dcc.Input(id='my-input', value='initial value', type='text')
    ]),
    html.Br(),
    html.Div(id='my-output'),

])


@app.callback(
    Output(component_id='my-output', component_property='children'),
    Input(component_id='my-input', component_property='value')
)
def update_output_div(input_value):
    return 'Output: {}'.format(input_value)


if __name__ == '__main__':
    app.run_server(debug=True)
```

![](https://gitlab.com/niltonmalves/dash_demo/-/raw/main/readme_images/callback.PNG)





------



###### Estrutura do código:

1. Carregar os dados

2. Criação do laytout da página:
   `html.Div(['o conteudo dá pagina, html.H1, dcc.Dropdown' deve ficar aqui dentro])`

   

3. chamar `@app.callback`que fará a atualização das informações no site.

   **IMPORTANTE**: o <u>output</u> é a saída da função de atualização, e o <u>input</u> é a entrada desta função.

   ![code](https://gitlab.com/niltonmalves/dash_demo/-/raw/main/readme_images/code.PNG)

[codigo da imagem acima](https://github.com/Coding-with-Adam/Dash-by-Plotly/blob/master/Other/Dash_Introduction/intro.py)

4. Criação da função de atualização, que será utilizada pelo `@app.callback`, nessa função utilizaremos os  **Plotly Graphs** por exemplo `import plotly.express as px `

   

 

# Dash Bootstrap Components

*dash-bootstrap-components* is a library of Bootstrap components for Plotly Dash, that makes it easier to build consistently styled apps with complex, responsive layouts

[Quickstart bootstrap](https://dash-bootstrap-components.opensource.faculty.ai/docs/)	

###### Referências:

> https://github.com/Coding-with-Adam/Dash-by-Plotly/tree/master/Other/Dash_Introduction

[video de tutorial](https://youtu.be/hSPmj7mK6ng)

