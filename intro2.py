import os
import pandas as pd
import plotly.express as px  # (version 4.7.0) # https://plotly.com/python/plotly-express/
import plotly.graph_objects as go

import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc  # Componentes INTERATIVOS dropdws, sliders.. https://dash.plotly.com/dash-core-components
import dash_html_components as html # para definir os formato do site https://dash.plotly.com/dash-html-components
from dash.dependencies import Input, Output # responsavel por receber e atualizar os dados do site

app = dash.Dash(__name__)
# ---------- Import and clean data (importing csv into pandas)
# df = pd.read_csv("intro_bees.csv")
df = pd.read_csv("https://raw.githubusercontent.com/Coding-with-Adam/Dash-by-Plotly/master/Other/Dash_Introduction/intro_bees.csv")

df = df.groupby(['State', 'ANSI', 'Affected by', 'Year', 'state_code'])[['Pct of Colonies Impacted']].mean()
df.reset_index(inplace=True)
print(df[:5])


# ------------------------------------------------------------------------------
# App layout
"""
https://dash.plotly.com/dash-html-components

html.Div([recebe lista com os componetes do site que será criado.])
"""
app.layout = html.Div([

    html.H1("Web Application Dashboards with Dash", style={'text-align': 'center'}),

    dcc.Dropdown(id="slct_year",
                 options=[
                     {"label": "2015", "value": 2015},
                     {"label": "2016", "value": 2016},
                     {"label": "2017", "value": 2017},
                     {"label": "2018", "value": 2018}],
                 multi=False,
                 value=2015,
                 style={'width': "40%"}
                 ),

    html.Div(id='output_container', children=[]),
    html.Br(),

   dcc.Graph(id='my_bee_map', figure={}),
    
])
#------------------------------------------------------------------------------
#Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container', component_property='children'),
     Output(component_id='my_bee_map', component_property='figure')
     ],
    [Input(component_id='slct_year', component_property='value')
    ]
)
def update_graph(year):
    print(year)
    print(type(year))
    cont = "The year chosen by user was: {}".format(year)
    dff = df.copy()
    dff = dff[dff["Year"] == year]
    dff = dff[dff["Affected by"] == "Varroa_mites"]
    # Plotly Express
    #https://plotly.com/python/maps/
    fig = px.choropleth(
        data_frame=dff,
        locationmode='USA-states',
        locations='state_code',
        scope="usa",
        color='Pct of Colonies Impacted',
        hover_data=['State', 'Pct of Colonies Impacted'],
        #color_continuous_scale=px.colors.sequential.YlOrRd,
        labels={'Pct of Colonies Impacted': "% of Beedd Colonies"},
        template='plotly_dark'
    )
    
    
    return cont, fig

if __name__ == '__main__':
    app.run_server(debug=True, port=os.getenv("PORT", "8055"))
